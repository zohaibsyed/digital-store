<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Article extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'articles';

    /**
     * get all articles by passing the article type id.
     * 
     *  @return App\Article
     */
    public static function getArticlesByType(int $id)
    {
        return DB::table('articles')
            ->join('article_types', 'article_types.id', '=', 'articles.article_type_id')
            ->select()
            ->Where('is_active', '=', 1)
            ->Where('article_types.id', '=', $id)
            ->orderBy('articles.created_at', 'DESC')
            ->get();
    }

    /**
     * Get all articles.
     * 
     * @return App\Article
     */
    public static function getAllArticles()
    {
        return DB::table('articles')
            ->join('article_types', 'article_types.id', '=', 'articles.article_type_id')
            ->select('articles.id as a_id', 'article_types.id as at_id', 'articles.created_at', 'article_name', 'article_types.name', 'price', 'image_name', 'image_path')
            ->Where('is_active', '=', 1)
            ->orderBy('articles.created_at', 'DESC')
            ->get();
    }
}
