<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class articlesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('articles')->insert([
            [
                'article_name' => 'Apple',
                'article_type_id' => 1,
                'description' => 'This is apple',
                'image_name' =>  'apple.jpg',
                'image_path' => 'public/assets/img/',
                'price' => 570,
                'is_active' => 1,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ],
            [
                'article_name' => 'Iphone',
                'article_type_id' => 1,
                'description' => 'This is Iphone',
                'image_name' =>  'iphone.jpg',
                'image_path' => 'public/assets/img/',
                'price' => 900,
                'is_active' => 1,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ],
            [
                'article_name' => 'iphone x',
                'article_type_id' => 1,
                'description' => 'This is iphone x',
                'image_name' =>  'iphonex.jpg',
                'image_path' => 'public/assets/img/',
                'price' => 600,
                'is_active' => 1,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ],
            [
                'article_name' => 'MacBook',
                'article_type_id' => 5,
                'description' => 'This is mac',
                'image_name' =>  'mac.jpg',
                'image_path' => 'public/assets/img/',
                'price' => 1900,
                'is_active' => 1,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ],
            [
                'article_name' => 'MacBook pro',
                'article_type_id' => 5,
                'description' => 'This is macbook pro',
                'image_name' =>  'macBook.jpg',
                'image_path' => 'public/assets/img/',
                'price' => 1600,
                'is_active' => 1,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ],
        ]);
    }
}
