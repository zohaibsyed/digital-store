@extends('backendLayouts.app')

@section('content')
<style>

</style>
<div class="container">
    <h4>Articles</h4>
<table id="article-table" class="table table-striped" style="width:100%">
        <thead>
            <tr>
                <th>Id</th>
                <th>Article Name</th>
                <th>Article Type</th>
                <th>Price</th>
                <th>Created At</th>
                <th>Image</th>
            </tr>
        </thead>
        <tbody>
            @foreach( $articles as $article )
            <tr>
                <td>{{ $article->a_id }}</td>
                <td>{{ $article->article_name }}</td>
                <td>{{ $article->name }}</td>
                <td>{{ number_format($article->price,2, '.', ',') }}</td>
                <td>{{ $article->created_at }}</td>
          
               <td><img src="{{ asset('public/assets/img') }}/{{$article->image_name}}" alt="" class="product-thumb"></td>
               
            </tr>
            @endforeach
            
         
        </tbody>

    </table>
</div>
<script>
    $(document).ready(function() {
    $('#article-table').DataTable();
} );
    </script>
@endsection

