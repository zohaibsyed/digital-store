<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Web routes -> No need to be login
Route::get('/', 'HomeController@index')->name('home');
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/getArticles/{id?}', 'ArticlesController@getArticles')->name('getArticles');
Route::get('/getAllArticle', 'ArticlesController@getAllArticle')->name('getAllArticle');


Auth::routes();

// admin portal routes-> need to be login first
Route::group(['middleware' => ['auth','AdminAuthentication']], function () {
    Route::get('/dashboard', 'AdminController@index')->name('dashboard');
});
